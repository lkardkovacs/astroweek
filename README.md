# Galaxy Morphology

Implementing Sérsic fitting and other methods to sort galaxies into morphological classes. <br>
The images analyzed are in the .FITS format. They were taken with a CCD camera through a Celestron CGE1400 telescope. <br>
Pictures taken 8-13.01.2024 at Berghaus Diavolezza.

## Status

Project finished. Final analysis was done in imfit.ipynb, which fits the Sersic profile directly to the picture with scipy, results are in Results_imfit. <br>
The reason for this is that statmorph was unable to handle most of our galaxies (about 2/3). quick_sersic.ipynb contains attempts <br>
to fit to the isophotes extracted from the pictures. The results of the naive algorithm are in the results folder, however attempts <br>
at more sophisticated implementations of the model in the same notebook did not succeed. The "difference" between imfit.ipynb and <br>
quick_sersic.ipynb is in Sersic.ipynb. Notebooks named after individual galaxies contain the data reduction for the respective galaxies. <br>
convenience_functions.py and gmorph.py contain functions used for visualization and data reduction respectively. <br>
plot.R was used to compare the lightcurves of galaxies visually. <br>

## Acknowledgment

-convenience_functions.py is from the Astropy webpage's CCD reduction and photometry guide <br>
https://github.com/astropy/ccd-reduction-and-photometry-guide/blob/main/notebooks/convenience_functions.py

Libraries/packages used: <br>

-ccdproc <br>
-astropy <br>
-photutils <br>
-astroalign <br>
-statmorph <br>
-scipy.optimize <br>
