# -*- coding: utf-8 -*-
"""
Created on Fri Dec 15 19:02:55 2023

@author: Levente Kardkovács
"""

#%%

import time
import os

from convenience_functions import show_image

import matplotlib.pyplot as plt
import numpy as np

import ccdproc as ccdp

from astropy.nddata import CCDData
from astropy.stats import mad_std
from astropy import units as u

from astropy.stats import sigma_clipped_stats, SigmaClip
from photutils.segmentation import detect_threshold, detect_sources
from photutils.utils import circular_footprint

import astroalign as aa

import statmorph
from statmorph.utils.image_diagnostics import make_figure
from astropy.modeling.models import Sersic2D
from astropy.visualization import simple_norm

#%%

def reduce(path, darkpath, obj=None, size=[3060, 2040],
           bias=False, dark=True, flat=True, show=False, export=True,
           dark_args = {'scl':5, 'sch':5, 'expt':None},
           flat_args = {'scl':5, 'sch':5, 'expt':None},
           sci_args = {'scl':5, 'sch':5, 'expt':None},
           bgsub_args = {'SCsig':3.0, 'SCiter':10, 'thsig':2.0, 'ssize':10, 'statsig':3.0}):
    """
    Perform data reduction on astronomical images.
    
    Returns reduced science frames (the bias, dark and flat frames, as well as the background
    have been subtracted) after importing raw science frames and the bias dark and flat frames.

    Parameters
    ----------
    path : str or tuple of str (of length 4)
        Name of the directory with the folders containing the images to use. If `obj` is not given
        or the path is not set up as explained in the `Notes` below, then pass a list with the names
        of directories for `bias`, `dark`, `flat` and `science` frames in this order.
        If any of `bias`, `dark` or `flat` are set to `False` then pass ``None'`` as the
        corresponding list item.
    darkpath : str
        Name of the directory containing the dark frames from the observing night when the galaxy was
        imaged.
    obj : str, optional
        Name of the folder containing images related to the astronomical object to analyze. 
        The default is ``None``, if `path` is given as a str, then this argument must be specified.
        See `Notes` for further details.
    size : array_like (of length 2), optional
        Maximum number of pixels along the x- and y-axis. The default is [3060, 2040]. This crops
        the right and top side of the picture at the indicated pixel numbers.
    bias : bool, optional
        If this is `True` bias frames are subtracted. The default is False.
    dark : bool, optional
        If this is `True` dark frames are subtracted. The default is True.
    flat : bool, optional
        If this is `True` flat frames are subtracted. The default is True.
    show : bool, optional
        If this is `True` the master bias, dark and flat frames are plotted along with a
        reduced science frame. The default is False.
    export : bool, optional
        If this is `True` the parameters passed to the data reduction steps are also returned. 
        The default is True.
    dark_args : dict
        Arguments passed to functions used in the dark subtraction procedure. The argument
        `expt`, the exposure time of the dark frames has to be set manually.
        The default is {'scl':5, 'sch':5, 'expt':None}. Call `tipps('reduced') for more details.
    flat_args : dict
        Arguments passed to functions used in the flat subtraction procedure. The argument
        `expt`, the exposure time of the flat frames has to be set manually.
        The default is {'scl':5, 'sch':5, 'expt':None}. Call `tipps('reduced') for more details
    sci_args : dict
        Arguments passed to functions used in the science frame reduction procedure. The argument
        `expt`, the exposure time of the science frames has to be set manually.
        The default is {'scl':5, 'sch':5, 'expt':None}. Call `tipps('reduced') for more details
    bgsub_args : dict, optional
        Arguments passed to functions used in the background subtraction procedure.
        The default is {'SCsig':3.0, 'SCiter':10, 'thsig':2.0, 'ssize':10, 'statsig':3.0}.
        Call `tipps('reduced') for more details

    Returns
    -------
    bg_sub : list
        A list of reduced and background subtracted science images.
    params : dict, optional
        If `export` is set to `True`, the list of parameters used for data reduction is also returned.
    
    Notes
    -----
    The function is most conveniently set up to pass a two str for the `path` and the 'darkpath'
    arguments, which are directories containing folders with the object names. 
    For the `obj` argument pass the name of the object that is to be analyzed. 
    In this folder subfolders named after the reduction steps shall contain the relevant .FITS files.
    
    Example: path = .../Astroweek24, obj = .../Astroweek24/NGC7331 and 
    darkpath = .../Astroweek24/Dark_100124.
    
    """
    
    # Crop image at these pixels
    x = size[0]; y = size[1]
    
    # Default way to import files used over the course of data reduction
    if isinstance(path, str):
        if obj is None:
            raise ValueError("Object directory name missing")
            
        else:
            #biasdir = ccdp.ImageFileCollection(path + "/" + obj + "/Bias")
            darkdir = ccdp.ImageFileCollection(path + "/" + darkpath)
            flatdir = ccdp.ImageFileCollection(path + "/" + "Flat")
            scidir = ccdp.ImageFileCollection(path + "/" + obj)
    
    # Manual import of files used over the course of data reduction
    else:
        biasdir = ccdp.ImageFileCollection(path[0])
        darkdir = ccdp.ImageFileCollection(path[1])
        flatdir = ccdp.ImageFileCollection(path[2])
        scidir = ccdp.ImageFileCollection(path[3])
    
    # TODO: implement overscan (potentially)
    
    t = time.time()
    # Creating the master bias
    if bias:
        # TODO: Implement bias subtraction
        raise NotImplementedError("Bias subtraction not yet implemented")
    tb = time.time()
    print('Master bias created in: %g s.' % (tb - t))
    
    t = time.time()  
    # Creating the master dark
    if dark:
        trimmed_darks = []

        for file_path in darkdir.files_filtered(include_path=True):
            ccd = CCDData.read(file_path, unit="count")
            trimmed_image = ccdp.trim_image(ccd[:, :x])
            trimmed_image = ccdp.trim_image(trimmed_image[:y, :])
            trimmed_darks.append(trimmed_image)

        master_dark = ccdp.combine(trimmed_darks,
                                 method='average',
                                 sigma_clip=True, 
                                 sigma_clip_low_thresh=dark_args['scl'], 
                                 sigma_clip_high_thresh=dark_args['sch'],
                                 sigma_clip_func=np.ma.median, 
                                 sigma_clip_dev_func=mad_std)
    td = time.time()
    print('Master dark created in: %g s.' % (td - t))
    
    t = time.time() 
    # Creating the master flat
    if flat:
        reduced_flats = []
        flatexp = flat_args['expt']
        darkexp = dark_args['expt']
        
        for file_path in flatdir.files_filtered(include_path=True):
            ccd = CCDData.read(file_path, unit="count")
            trimmed_image = ccdp.trim_image(ccd[:, :x])
            trimmed_image = ccdp.trim_image(trimmed_image[:y, :])
            flat_reduced = ccdp.subtract_dark(trimmed_image, master_dark, 
                                              dark_exposure=darkexp*u.second, 
                                              data_exposure=flatexp*u.second, 
                                              exposure_unit=u.second, 
                                              scale=True)
            reduced_flats.append(flat_reduced)
    
        def inv_median(a):
            return 1 / np.median(a)

        master_flat = ccdp.combine(reduced_flats, method='average', 
                                   scale=inv_median, 
                                   sigma_clip=True, 
                                   sigma_clip_low_thresh=flat_args['scl'], 
                                   sigma_clip_high_thresh=flat_args['sch'],
                                   sigma_clip_func=np.ma.median, 
                                   signma_clip_dev_func=mad_std)
    tf = time.time()
    print('Master flat created in: %g s.' % (tf - t))
    
    red_sc = []
    raw_sc = []
    
    t = time.time()
    # Subtracting the master bias, dark and flat from the raw science frames, whichever is applicable
    for file_path in scidir.files_filtered(include_path=True):
        # Read the image using CCDData
        ccd = CCDData.read(file_path, unit="count")
        raw_sc.append(ccd)
        reduced = ccdp.trim_image(ccd[:, :x])
        reduced = ccdp.trim_image(reduced[:y, :])
        
        if bias:
            # TODO
            raise NotImplementedError("Bias subtraction not yet implemented")
            #reduced = ccdp.subtract_bias(reduced, master_bias)

        if dark:
            sciexp = sci_args['expt']
            darkexp = dark_args['expt']
            reduced = ccdp.subtract_dark(reduced, master_dark, 
                                         dark_exposure=darkexp*u.second, 
                                         data_exposure=sciexp*u.second,
                                         exposure_unit=u.second, 
                                         scale=True)
        if flat:
            reduced = ccdp.flat_correct(reduced, master_flat)
    
        red_sc.append(reduced)
    tred = time.time()
    print('Reduced science frames created in: %g s.' % (tred - t))
    
    # TODO: implement preliminary masks/cosmic ray detection (potentially)
        
    bg_sub = []
    
    t = time.time()
    # Perform background subtraction on the reduced science frames
    for im in red_sc:
    
        copy = im.copy()
        data = im.data

        sigma_clip = SigmaClip(sigma=bgsub_args['SCsig'], maxiters=bgsub_args['SCiter'])
        threshold = detect_threshold(data, nsigma=bgsub_args['thsig'], sigma_clip=sigma_clip)
        segment_img = detect_sources(data, threshold, npixels=bgsub_args['ssize'])
        footprint = circular_footprint(radius=bgsub_args['ssize'])
        mask = segment_img.make_source_mask(footprint=footprint)
        mean, median, std = sigma_clipped_stats(data, sigma=bgsub_args['statsig'], mask=mask)

        sub = data-mean
        sub[sub < 0] = 0
        copy.data = sub
    
        bg_sub.append(copy)
    tbg = time.time()
    print('Background subtraction in: %g s.' % (tbg - t))
    
    
    # Plot the master bias, dark and flats along with a reduced background subtracted science frame    
    if show:
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(20, 20))
        
        if bias:
            #show_image(master_bias.data, cmap='gray', ax=ax1, fig=fig)
            #ax1.set_title('Master bias')
            raise NotImplementedError("Bias plot not implemented")
        
        if dark:
            show_image(master_dark.data, cmap='gray', ax=ax2, fig=fig)
            ax2.set_title('Master dark')
        
        if flat:
            show_image(master_flat.data, cmap='gray', ax=ax3, fig=fig)
            ax3.set_title('Master flat')

        show_image(bg_sub[0].data, cmap='gray', ax=ax4, fig=fig)
        ax4.set_title('Reduced science frame (background subtracted)')
    
    # Export the parameters used for data reduction so they can be saved more easily later
    if export:
        # Create an object to store the parameters in
        params = {'size':size, 'bias':bias, 'dark':dark, 'flat':flat,
                  'dark_args':dark_args, 'flat_args':flat_args,
                  'sci_args':sci_args, 'bgsub_args':bgsub_args}
        return bg_sub, params
    
    return bg_sub
    
#%%    
    
def combine(images, method="median", dsig=5e5, show=False, export=True):
    """
    Aligns astronomical images and combines them by taking their mean or median.
    
    Returns the combined image.

    Parameters
    ----------
    images : tuple of CCDData objects
        Images to be aligned and combined.
    method : str, optional
        Method with which to combine the aligned images. The options are `mean`, `median` or both.
        In the latter case the combined image obtained via both methods is returned.
        The default is "median".
    dsig : float, optional
        The number of standard deviations above which a source in the image is used as a
        reference object for aligning the images. The default is 5e5.
    show : bool, optional
        If set to `True` pre- and post-combination images are plotted. The default is False.
    export : bool, optional
        If set to `True` then the parameters of the alignment and combination are also returned. 
        The default is True.

    Returns
    -------
    final_sc (or final_mean and final_med) : 2D numpy array
        A single image created from the aligned and combined reduced and background subtracted
        science frames.
        
    params : dict, optional
        If `export` is set to `True`, the list of parameters used for alignment and combination 
        is also returned.

    """
    
    if not(method == "both" or method == "mean" or method == "median"):
        raise ValueError("Invalid method")
    
    # Use the first image as the reference
    copy = images[0].copy()
    ref_im = copy.data

    aligned_im = [ref_im]

    t = time.time()
    # Align images to the first image
    for im in images[1:]:
        
        copy = im.copy().data
        im_aligned, footprint = aa.register(copy, ref_im, detection_sigma=dsig)
        aligned_im.append(im_aligned)
    
    ta = time.time()
    print('Images aligned in: %g s.' % (ta - t))
    
    t = time.time()
    # Combining images with both methods
    if method == "both":
        final_mean = np.mean(aligned_im, axis = 0)
        final_med = np.median(aligned_im, axis = 0)
        
        tboth = time.time()
        print('Images combined in: %g s.' % (tboth - t))

        # Compare the combined images created with the two methods
        if show:
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))

            show_image(final_mean.data, cmap='gray', ax=ax1, fig=fig)
            ax1.set_title('Aligned-combined (mean)')

            show_image(final_med.data, cmap='gray', ax=ax2, fig=fig)
            ax2.set_title('Aligned-combined (median)')
        
        # Export the parameters used for alignment and combination so they can be saved more easily later
        if export:
            # Create an object to store the parameters in
            params = {'method':method, 'dsig':dsig}
            return final_mean, final_med, params
        
        return final_mean, final_med    
    
    # Combine images by taking their mean OR their median
    else:
        t = time.time()
        if method == "mean":
            final_sc = np.mean(aligned_im, axis = 0)
            tmean = time.time()
            print('Images combined in: %g s.' % (tmean - t))
        
        t = time.time()
        if method == "median":
            final_sc = np.median(aligned_im, axis = 0)
            tmedian = time.time()
            print('Images combined in: %g s.' % (tmedian - t))
        
        # Show a pre- and post-combination image
        if show:
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))

            show_image(ref_im, cmap='gray', ax=ax1, fig=fig)
            ax1.set_title('Pre-combined')

            show_image(final_sc.data, cmap='gray', ax=ax2, fig=fig)
            ax2.set_title('Aligned-combined' + ' (' + method + ')')
        
        # Export the parameters used for alignment and combination so they can be saved more easily later
        if export:
            # Create an object to store the parameters in
            params = {'method':method, 'dsig':dsig}
            return final_sc, params
        
        return final_sc
    
#%%

def fit(image, loc=np.array([[0, 3060], [0, 2040]]), 
        sersic=True, sersic2D=False, isophote=False, export=True, 
        show=["cutout", "segmap"],
        segmap_args={'thsig':1.0, 'cpix':10000},
        bgsub_args = {'SCsig':3.0, 'SCiter':10, 'thsig':2.0, 'ssize':10, 'statsig':3.0},
        sersic_args = {'gain':1, 'Rratio':2.0, 'maxiter':100}):
    """
    Performs a one or two component Sérsic fit, or calculates the isophotes or ellipticities
    of galaxies.

    Parameters
    ----------
    image : 2D-array
        A clean (reduced and background subtracted) image of a galaxy.
    loc : 2D-array, optional
        A 2x2 matrix containing the pixels at which to crop the picture. The goal is to zoom in on
        the object of interest. The first row contains the x-coordinates, the second row contains
        the y-coordinates. The default is [[0, 3060], [0, 2040]].
    sersic : bool, optional
        If this is `True` the Sérsic index of the galaxy is calculated. The default is True.
    sersic2D : bool, optional
        If this is `True` the two component Sérsic fit is calculated along the one component fit. 
        The default is False.
    isophote : bool, optional
        If this is `True` the isophotes of a galaxy are calculated. The default is False.
    export : bool, optional
        If this is `True` the parameters passed to the fitting algorithms are also returned. 
        The default is True.
    show : list of str, optional
        Plot the images corresponding to the passed keywords. Accepted keywords are `cutout`,
        `segmap`, `bgsub`, `sersic`, `sersic2D`. The default is ["cutout", "segmap"].
    segmap_args : dict, optional
        Arguments passed to the functions creating the segmentation map. 
        The default is {'thsig':1.0, 'cpix':10000}.
    bgsub_args : dict, optional
        Arguments passed to the functions performing background subtraction on the images. 
        The default is {'SCsig':3.0, 'SCiter':10, 'thsig':2.0, 'ssize':10, 'statsig':3.0}.
    sersic_args : dict, optional
        Arguments passed to the function calculating the Sérsic fit. 
        The default is {'gain':1, 'maxiter':100}.

    Returns
    -------
    morph : SourceMorphology object of the statmorph.statmorph module
        An object containing the results of the Sérsic fitting.
        
    params : dict, optional
        If `export` is set to `True`, the list of parameters used for fitting is also returned.
        
    res : dict, optional
        If `export` is set to `True`, the results of the fit are also returned explicitly.
        

    """
    # TODO: pass the show and isophote docstring to the isophote code

    # Set the coordinates for cropping the image
    xl = loc[0, 0]; xh = loc[0, 1]; yl = loc[1, 0]; yh = loc[1, 1]
    
    # TODO: implement isophotes
    if isophote:
        raise NotImplementedError("Isophotes not yet implemented")
    
    # Crop the image
    cutout = image[yl:yh, :]
    cutout = cutout[:, xl:xh]
    
    # Plot cropped image
    if any(s == "cutout" for s in show):
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        show_image(cutout, cmap="gray", ax=ax, fig=fig)
        ax.set_title('Cropped image')
        
    t = time.time()
    # Create segmentation map
    threshold = detect_threshold(cutout, segmap_args['thsig']) # From how many std-s an object is considered a source
    npixels = segmap_args['cpix']  # minimum number of connected pixels
    segmap = detect_sources(cutout, threshold, npixels)
    tseg = time.time()
    print('Segmentation map created in: %g s.' % (tseg - t))
    
    # Plot segmentation map
    if any(s == "segmap" for s in show):
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        show_image(segmap, cmap="gray", ax=ax, fig=fig)
        ax.set_title('Segmentation map')
        
    t = time.time()
    # Subtract the background from the image
    sigma_clip = SigmaClip(sigma=bgsub_args['SCsig'], maxiters=bgsub_args['SCiter'])
    threshold = detect_threshold(cutout, nsigma=bgsub_args['thsig'], sigma_clip=sigma_clip)
    segment_img = detect_sources(cutout, threshold, npixels=bgsub_args['ssize'])
    footprint = circular_footprint(radius=bgsub_args['ssize'])
    mask = segment_img.make_source_mask(footprint=footprint)
    mean, median, std = sigma_clipped_stats(cutout, sigma=bgsub_args['statsig'], mask=mask)

    sub = cutout-mean
    sub[sub < 0] = 0
    im_to_fit = sub
    tbg = time.time()
    print('Background subtraction in: %g s.' % (tbg - t))
    
    # Plot the background subtracted image
    if any(s == "bgsub" for s in show):
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        show_image(im_to_fit, cmap="gray", ax=ax, fig=fig)
        ax.set_title('Background subtracted image')
        
    t = time.time()
    # Perform Sérsic fitting and print the solution
    if sersic:
        print("Sérsic fitting in progress, this may take a while.")
        source_morphs = statmorph.source_morphology(im_to_fit, 
                                                    segmap, gain=sersic_args['gain'], 
                                                    include_doublesersic=sersic2D, 
                                                    sersic_fitting_args={'maxiter':sersic_args['maxiter']},
                                                    doublesersic_rsep_over_rhalf=sersic_args['Rratio'],
                                                    doublesersic_fitting_args={'maxiter':sersic_args['maxiter']})
        morph = source_morphs[0]
        ts = time.time()
        print('Sérsic fit found in: %g s.' % (ts - t))
        print()
        print("SINGLE COMPONENT SÉRSIC FIT")
        print('Sérsic index (n)={:.3f}'.format(morph.sersic_n))
        print('Ellipticity of the Sérsic fit={:.3f}'.format(morph.sersic_ellip))
        print('Half-light radius={:.0f}'.format(morph.sersic_rhalf))
        print('Chi-squared={:.3f}'.format(morph.sersic_chi2_dof))
        print()
        if morph.flag > 1:
            print("Morphological fit unsuccessful. Modify the input image or the parameters passed!")
        else:
            if morph.flag == 1:
                print("Low quality morphological fit. Maybe try slightly tweaking the image and the parameters.")
            if morph.flag == 0:
                print("Good morphological fit found.")
        
        if morph.flag_sersic > 1:
            print("Sérsic fit unsuccessful. Modify the input image or the parameters passed!")
        else:
            if morph.flag_sersic == 1:
                print("Low quality Sérsic fit. Maybe try slightly tweaking the image and the parameters.")
            if morph.flag_sersic == 0:
                print("Good Sérsic fit found.")
                
        if any(s == "sersic" for s in show):
            make_figure(morph)


        if sersic2D:
            print()
            print("TWO COMPONENT SÉRSIC FIT")
            print('First Sérsic index (n1)={:.3f}'.format(morph.doublesersic_n1))
            print('Second Sérsic index (n2)={:.3f}'.format(morph.doublesersic_n2))
            print('Ellipticity of first fit={:.3f}'.format(morph.doublesersic_ellip1))
            print('Ellipticity of the second fit={:.3f}'.format(morph.doublesersic_ellip2))
            print('First half-light radius={:.0f}'.format(morph.doublesersic_rhalf1))
            print('Second half-light radius={:.0f}'.format(morph.doublesersic_rhalf2))
            print('Chi-squared={:.3f}'.format(morph.doublesersic_chi2_dof))
            print()
            if morph.flag_doublesersic > 1:
                print("Two component Sérsic fit unsuccessful. Modify the input image or the parameters passed!")
            else:
                if morph.flag_doublesersic == 1:
                    print("Low quality two component Sérsic fit. Maybe try slightly tweaking the image and the parameters.")
                if morph.flag_doublesersic == 0:
                    print("Good two component Sérsic fit found.")
                    
            if any(s == "sersic2D" for s in show):
                ny, nx = cutout.shape
                Y, X = np.mgrid[0:ny, 0:nx]
                sersic1 = Sersic2D(morph.doublesersic_amplitude1,
                                   morph.doublesersic_rhalf1,
                                   morph.doublesersic_n1,
                                   morph.doublesersic_xc,
                                   morph.doublesersic_yc,
                                   morph.doublesersic_ellip1,
                                   morph.doublesersic_theta1)
                sersic2 = Sersic2D(morph.doublesersic_amplitude2,
                                   morph.doublesersic_rhalf2,
                                   morph.doublesersic_n2,
                                   morph.doublesersic_xc,
                                   morph.doublesersic_yc,
                                   morph.doublesersic_ellip2,
                                   morph.doublesersic_theta2)
                image1 = sersic1(X, Y)
                image2 = sersic2(X, Y)
                image_total = image1 + image2

                fig = plt.figure(figsize=(15,5))
                ax = fig.add_subplot(131)
                ax.imshow(image1, cmap='gray', origin='lower',
                          norm=simple_norm(image_total, stretch='log', log_a=10000))
                ax.set_title('First component')
                ax.text(0.04, 0.93, 'n1 = %.4f' % (morph.doublesersic_n1,),
                        bbox=dict(facecolor='white'), transform=ax.transAxes)
                ax = fig.add_subplot(132)
                ax.imshow(image2, cmap='gray', origin='lower',
                          norm=simple_norm(image_total, stretch='log', log_a=10000))
                ax.set_title('Second component')
                ax.text(0.04, 0.93, 'n2 = %.4f' % (morph.doublesersic_n2,),
                        bbox=dict(facecolor='white'), transform=ax.transAxes)
                ax = fig.add_subplot(133)
                ax.imshow(image_total, cmap='gray', origin='lower',
                          norm=simple_norm(image_total, stretch='log', log_a=10000))
                ax.set_title('Composite model')
            
            # Export the parameters used for data reduction and fit so they can be saved more easily later
            if export:
                # Create an object to store the parameters in
                params = {'loc':loc, 'sersic':sersic, 'sersic2D':sersic2D, 'isophote':isophote,
                          'segmap_args':segmap_args, 'bgsub_args':bgsub_args,
                          'sersic_args':sersic_args}
                # Create an object with the results of the one and two component fits
                res = {'Sérsic index (single)':morph.sersic_n,
                       'Ellipticity of single fit': morph.sersic_ellip,
                       'Chi-squared (single)':morph.sersic_chi2_dof,
                       'Morph quality':morph.flag, 'Fit quality (single)':morph.flag_sersic,
                       'Sérsic index (inner)': morph.doublesersic_n1,
                       'Sérsic index (outer)': morph.doublesersic_n2,
                       'Ellipticity of inner fit': morph.doublesersic_ellip1,
                       'Ellipticity of outer fit': morph.doublesersic_ellip2,
                       'Chi-squared (double)': morph.doublesersic_chi2_dof,
                       'Fit quality (double)': morph.flag_doublesersic}
                return morph, params, res
            
            return morph
        
        # Export the parameters used for data reduction and fit so they can be saved more easily later
        if export:
            # Create an object to store the parameters in
            params = {'loc':loc, 'sersic':sersic, 'sersic2D':sersic2D, 'isophote':isophote,
                      'segmap_args':segmap_args, 'bgsub_args':bgsub_args,
                      'sersic_args':sersic_args}
            # Create an object with the results of the one component fit
            res = {'Sérsic index':morph.sersic_n, 'Chi-squared':morph.sersic_chi2_dof,
                   'Morph quality':morph.flag, 'Fit quality':morph.flag_sersic}
            return morph, params, res
        
        return morph
        
    
    print("No fit method initialized")
    return None

#%%

def save(obj, fname, typ="text", to=os.getcwd()):
    """
    Exports an image or the contents of a dictionary to a specified folder.

    Parameters
    ----------
    obj : dict or 2D-array
        The object to export.
    fname : str
        The name of the file to be created.
    typ : str, optional
        The kind of data to be exported. Accepted arguments are `image` and `text`.
        The default is "text".
    to : str, optional
        The folder where the export file is to be created. The default is the current working
        directory (i.e. the location of the script where `save` is called.

    """

    
    if not(typ == "image" or typ == "text"):
        raise ValueError("Invalid object to export")
    
    if typ == "image":
        # TODO
        file_path = to + fname + '.png'
        plt.imsave(file_path, obj, cmap='gray')
        #raise NotImplementedError("Image export not yet implemented")
    
    if typ == "text":
        file_path = fname + '.txt'

        with open(to + "/" + file_path, 'w') as file:
            for key, value in obj.items():
                file.write(f'{key}: {value}')
                
    return None

#%%

def tipps(step):
    """
    Explanation of the parameters passed to the different data reduction and fitting steps.
    Suggestions for tweaking the values of the parameters.

    Parameters
    ----------
    step : str
        The name of the function that the parameters are passed to. The accepted values are
        `reduce`, `combine` and `fit`.

    """
    # TODO: implement
    
    if not(step=="reduce" or step=="combine" or step=="fit"):
        raise ValueError("Invalid analysis step")
    
    if step=="reduce":
        to_print = """
        ## Arguments of the reduce function \n
        # size \n
        This argument exists because the picture is bigger than the amount of pixels the camera has. 
        By default the it is set to the number of pixels the CCD has according to the manual \n
        # dark_args
        -scl, sch: When combining the dark frames certain pixels can be rejected. With these arguments 
        one can specify the number of standard deviations below and above which a pixel should be rejected. 
        Increase the values to exclude more pixels, decrease it to exclude less. \n
        -expt: The exposure time in seconds with which the dark frames have been taken. It can be found in the header of the FITS file, 
        but it is not read from there automatically to avoid errors (they do not always seem trustworthy based on the example data),
        so it has to be entered manually. \n
        # flat_args \n
        -scl, sch: When combining the flat field frames certain pixels can be rejected. With these arguments one
        can specify the number of standard deviations below and above which a pixel should be rejected. 
        Increase the values to exclude more pixels, decrease it to exclude less.\n
        -expt: The exposure time in seconds with which the flat field frames have been taken. It can be found in the header of the FITS file,
        but it is not read from there automatically to avoid errors (they do not always seem trustworthy based on the example data),
        so it has to be entered manually. \n
        # sci_args \n
        scl, sch: When combining the science frames certain pixels can be rejected. With these arguments one
        can specify the number of standard deviations below and above which a pixel should be rejected. 
        Increase the values to exclude more pixels, decrease it to exclude less. \n
        -expt: The exposure time in seconds with which the science frames have been taken. It can be found in the header of the FITS file,
        but it is not read from there automatically to avoid errors (they do not always seem trustworthy based on the example data),
        so it has to be entered manually. \n
        # bgsub_args \n
        -SCsig: Controls which parts of the picture to reject. One has to specify the number of standard deviations
        above and below which pixels are rejected. Increasing the value leads to fewer rejected pixels. \n
        -SCiter: Creating the "clip-map", i.e. the array specifying which pixels to reject is an iterative procedure. 
        This parameter gives the maximum number of iterations. If the data reduction was effective, then this number need not be too high. \n
        -thsig: Specify the number of standard deviations from the background above which
        a pixel is considered part of a source. Increasing the value decreases the number of sources. \n
        -ssize: This controls how big a bright group of pixels has to be to considered a source. One has to specify the number of pixels.
        A subtlety in the implementation is that this is actually passed to two different functions which have similar size parameters.
        One needs the number of pixels, the other the radius. For small values (it is sensible to keep it in the 10-100 range) they are
        approximately the same. \n
        -statsig: Similarly to other 'sig' parameters this specifies the number of standard deviations at which the data is clipped.
        This particular argument is not passed to a function that transforms the data, it merely computes the statistics of the clipped data,
        in particular the mean median and standard deviaton.
        """
        print(to_print)
        
    if step=="combine":
        to_print = """
        ## Arguments of the combine function \n
        # method \n
        This argument decides if the mean or the median should be used to combine the reduced and aligned science frames.
        Why not just always use the mean? If the pictures are not perfectly aligned, then streaks can appear in the combine pictures.
        Essentially some stars might get smeared when combining. The median is a more robust way to combine because it is insensitive
        to a few misaligned pictures as long as the majority are properly aligned. \n
        # dsig \n
        This argument specifies the number of standard deviations above which a star (conceptually, more accurately a pixel) is used
        to guide the alignment. By default it is set to 50 000 sigmas. The reason for this is that we really only want a handful of
        pixels to be used for alignment (the function will throw an error if detects too many guiding sources). Increase the value to
        decrease the number of sources used for alignment, decrease it to increase the number of sources used for alignment.
        """
        print(to_print)
      
    if step=="fit":
        to_print = """
        ## Arguments of the fit function \n
        # loc \n
        This argument enables one to zoom in on a certain part of the picture. The goal is to decrease the number of sources
        in the picture that are not the galaxy to be fitted. This makes it easier to set the arguments for the background
        subtraction and the segmentation map because then one essentially only has to consider the galaxy and the background. \n
        # segmap_args \n
        -thsig: This sets the threshold for the segmentation map, above which a pixel is considered part of a source. Specifically
        one has to provide the number of standard deviations. Set it as low as possible without picking up the background to capture
        all features of a galaxy (especially spirals). \n
        -cpix: The minimum number of pixels above the threshold that need to be connected to each other for this group of pixels to be
        considered an object. Since galaxies are extended objects it is safe to set this to a high number 
        (order: 10^3-10^5 depending on how big the galaxy is) so that stars (that do not overlap with the galaxy) are not picked up. \n
        # bgsub_args \n
        -SCsig: Controls which parts of the picture to reject. One has to specify 
        the number of standard deviations above and below which pixels are rejected. Increasing the value leads to fewer rejected pixels. \n
        -SCiter: Creating the "clip-map", i.e. the array specifying which pixels to reject is an iterative procedure. 
        This parameter gives the maximum number of iterations. If the data reduction was effective, then this number need not be too high. \n 
        -thsig: Specify the number of standard deviations from the background above which a pixels is considered part of a source. 
        Increasing the value decreases the number of sources. \n 
        -ssize: This controls how big a bright group of pixels has to be to considered a source. One has to specify the number of pixels.
        A subtlety in the implementation is that this is actually passed to two different functions which have similar size parameters.
        One needs the number of pixels, the other the radius. For small values (it is sensible to keep it in the 10-100 range) they are
        approximately the same. \n
        -statsig: Similarly to other 'sig' parameters this specifies the number of standard deviations at which the data is clipped.
        This particular argument is not passed to a function that transforms the data, it merely computes the statistics of the clipped data,
        in particular the mean median and standard deviaton. \n
        # sersic_args \n
        -gain: The amplification of the CCD. Pixel value = gain*electron count. In general no need to change it from the default 1.0. \n 
        -Rratio: This specifies the radius where the inner and outer Sérsic fits should be separated in case of two component fitting. One
        has to specify this value as a multiple of the half-light semimajor axis of the one component fit (morph.rhalf_ellip), i.e.
        separation radius = Rratio*h-l semim. axis. This is used as an initial guess for the iterative algorithm computing the Sérsic fits.
        CAUTION: The algorithm can be EXTREMELY sensitive to this parameter, 
        changing it by .05 can make the fit unstable (result in nonsensical or inconsistent (different each time the fit runs) results). 
        Try different values until the results can be replicated. \n
        -maxiter: The maximum number of iterations the fitting algorithm can do. When two component fitting is done it is best to set this
        value higher (1000-10000). It is best to run the fit function with warnings on,
        and a lower value of this parameter to see if the algorithm reaches the maximum number of iterations.
        """
        print(to_print)
        
    return None
#%%
def show():
    # TODO: implement visualization of morph objects so fit doesn't always have to be run
    return None
