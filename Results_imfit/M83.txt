{'Crop_pix': 'xmin = 900, xmax = 2800, ymin = 700, ymax = 2700', 
'Geom_args': 'x0 = 975, y0 = 1150, sma = 500, eps = 0.50, pa = -45', 
'Geom_args2': 'sma_0 = 100, minsma_ = 10, maxsma = 600, st = 2, lin = True', 
'Initial_params': 'n0 = 1, Ie0 = 100, Re0 = 70', 
'Results': 'n = 4.517 pm 0.016, Ie = 3.078 pm 0.052, Re = 1096.714 pm 11.995', 
'r-square': 'mean = 1372, median = 4.1', 
'1D r-square': 'mean = 1432, median = 13.6'}