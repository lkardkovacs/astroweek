{'Crop_pix': 'xmin = 1700, xmax = 2400, ymin = 1300, ymax = 2100', 
'Geom_args': 'x0 = 350, y0 = 445, sma = 60, eps = 0.50, pa = 95', 
'Geom_args2': 'sma_0 = 30, minsma_ = 5, st = 2, lin = True', 
'Initial_params': 'n0 = 4, Ie0 = 100, Re0 = 40', 
'Results': 'n = 1.713 pm 0.002, Ie = 68.536 pm 0.129, Re = 80.220 pm 0.092', 
'r-square': 'mean = 58, median = 4.2', 
'1D r-square': 'mean = 310, median = 9.6'}