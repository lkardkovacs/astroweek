{'Crop_pix': 'xmin = 700, xmax = 3500, ymin = 100, ymax = 3500', 
'Geom_args': 'x0 = 1400, y0 = 1500, sma = 1400, eps = 0.80, pa = -55', 
'Geom_args2': 'sma_0 = 500, minsma_ = 10, maxsma = 1500, st = 4, lin = True', 
'Initial_params': 'n0 = 1, Ie0 = 50, Re0 = 200', 
'Results': 'n = 2.435 pm 0.049, Ie = 3.549 pm 0.198, Re = 2234.565 pm 82.750', 
'r-square': 'mean = 29448, median = 6.8', 
'1D r-square': 'mean = 29452, median = 2.3'}