par.def = par()

lcurve = function (I.e, n, ...) {
  b.n = 2*n-1/3
  curve(I.e*exp(-b.n*((x)^(1/n)-1)), 0, 0.5,
        xlab=expression(R/R[eff]), ylab="Intensity (arb.)",
        main="Intensity profiles", log="y", ...)
  legend("topright", c("Ellipticals", "Spirals"), lty=c(1, 2))
  text(c(0.01, 0.05), c(3.5, 1000), labels=c("n=1", "n=4"), col=c(3, 2))
}

lcurve2 = function (mu.e, n, ...) {
  b.n = 2*n-1/3
  curve(mu.e + 2.5*b.n/log(10) * (x^(1/n)-1), 0, 3,
        xlab=expression(R/R[eff]), ylab="surface brightness (arb.)",
        main="Surface brightness profiles", ...)
}

lcurve(I.e=1, n=4, add=FALSE, col=2, lty=1, lwd=4) # lty=1 for ellipticals
lcurve(I.e=1, n=1, add=TRUE, col=3, lty=2, lwd=4) # lty=2 for spirals

lcurve(I.e=1, n=1.963, add=TRUE, col=1, lty=1, lwd=1.5) # M32
lcurve(I.e=1, n=2.601, add=TRUE, col=1, lty=2, lwd=1.5) # M33
lcurve(I.e=1, n=2.021, add=TRUE, col=1, lty=1, lwd=1.5) # M49
lcurve(I.e=1, n=3.639, add=TRUE, col=1, lty=2, lwd=1.5) # M51
lcurve(I.e=1, n=4.937, add=TRUE, col=1, lty=2, lwd=1.5) # M74 (no chi-square)
lcurve(I.e=1, n=3.272, add=TRUE, col=1, lty=2, lwd=1.5) # M81
lcurve(I.e=1, n=4.517, add=TRUE, col=1, lty=2, lwd=1.5) # M83
lcurve(I.e=1, n=1.673, add=TRUE, col=1, lty=1, lwd=1.5) # M87
lcurve(I.e=1, n=1.969, add=TRUE, col=1, lty=2, lwd=1.5) # M95 (non chi-square)
lcurve(I.e=1, n=2.874, add=TRUE, col=1, lty=1, lwd=1.5) # M110
lcurve(I.e=1, n=2.435, add=TRUE, col=1, lty=2, lwd=1.5) # NGC253 (flagged fit)
lcurve(I.e=1, n=1.854, add=TRUE, col=1, lty=1, lwd=1.5) # NGC1407
lcurve(I.e=1, n=1.713, add=TRUE, col=1, lty=1, lwd=1.5) # NGC4125


#M83, NGC1407 I should be able to make work
#Retry M106
#M74, M95 fit seems reasonable visually but the chi-squared is bonkers;
  #NGC253 seems reasonable too but it was flagged by the software


